# README #

Example profile headers for CooklyBookly author profile pages.

There is currently only one example but we will soon be adding more.


### What should I do ###
A profile header is a very simple pair of HTML and CSS/SCSS files. You can develop this very easily on your local machine.

Use any code editor you like and just open up the html file in a browser to preview.

Once you've tested it, you just copy and paste into your CooklyBookly author profile settings.

### CooklyBookly documentation
[Click here](https://docs.cooklybookly.com/en/articles/4086806-driving-leads-and-sales-from-your-profile-page) to find the CooklyBookly documentation on how to configure a profile header.

